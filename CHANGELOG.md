# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Fixed

- fix: change gpg key

## v2.1.0 - 2021-08-22

### Added

- test: add support debian 11

### Fixed

- fix: bug with multiple privileges

### Changed

- chore: use FQCN for module name
- fix: replace no_log to loop label
- test: use personal docker registry

## v2.0.1 - 2020-03-28

### Changed

- fix: remove default value for mariadb_password
- test: replace kitchen to molecule

## v2.0.0 - 2019-06-05

- breaking: remove support for ansible < 2.8
- feat: add official repository
- feat: remove unused users in init
- feat: install database if datadir doesn't exist

## v1.0.1 - 2019-03-16

- fix: add client-server option in default configuration
- fix: install python-mysqldb package
- test: add tests with travis-ci

## v1.0.0 - 2019-02-23

- first version
